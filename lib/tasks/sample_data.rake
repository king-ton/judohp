# encoding: utf-8

namespace :db do
  desc "Datenbank mit Beispieldaten füllen"
  task populate: :environment do
    admin = User.create!( name: "Beispiel-Benutzer",
                          email: "example@tonifreitag.de",
                          password: "foobar",
                          password_confirmation: "foobar")
    admin.toggle!(:admin)
                  
    99.times do |n|
      name = Faker::Name.name
      email = "example-#{n+1}@tonifreitag.de"
      password = "password"
      User.create!(name: name, email: email, password: password, password_confirmation: password)
    end
  end
end