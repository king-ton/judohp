require 'spec_helper'

describe "Static Pages" do

  subject { page }

  shared_examples_for "Alle statische Seiten" do
    it { should have_selector('h1', text: heading) }
    it { should have_selector('title', text: full_title(page_title)) }
  end

  describe "Startseite" do
    before { visit root_path }

    let(:heading) { 'Startseite' }
    let(:page_title) { '' }

    it_should_behave_like "Alle statische Seiten"
    it { should_not have_selector 'title', text: '| Home' }
  end

  describe "Hilfe-Seite" do
    before { visit hilfe_path }

    let(:heading) { 'Hilfe' }
    let(:page_title) { 'Hilfe' }

    it_should_behave_like "Alle statische Seiten"
  end

  describe "Impressum" do
    before { visit impressum_path }

    let(:heading) { 'Impressum' }
    let(:page_title) { 'Impressum' }

    it_should_behave_like "Alle statische Seiten"
  end

  describe "Kontakt" do
    before { visit kontakt_path }

    let(:heading) { 'Kontakt' }
    let(:page_title) { 'Kontakt' }

    it_should_behave_like "Alle statische Seiten"
  end

  it "Richtige Links auf dem Layout" do
    visit root_path
    click_link "Impressum"
    page.should have_selector 'title', text: full_title('Impressum')
    click_link "Hilfe"
    page.should have_selector 'title', text: full_title('Hilfe')
    click_link "Kontakt"
    page.should have_selector 'title', text: full_title('Kontakt')
    click_link "Startseite"
    click_link "Jetzt Registrieren!"
    page.should have_selector 'title', text: full_title('Registrierung')
    click_link "Judo-Homepage"
    page.should have_selector 'title', text: full_title('')
  end
end

