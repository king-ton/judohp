# coding: utf-8

require 'spec_helper'

describe "Benutzer-Seite" do

  subject { page }

  describe "index" do

    let(:user) { FactoryGirl.create(:user) }

    before(:each) do
      sign_in user
      visit users_path
    end

    it { should have_selector('title', text: 'Alle Benutzer') }
    it { should have_selector('h1', text: 'Alle Benutzer') }

    describe "Paginierung" do

      it { should have_selector('div.pagination') }

      it "soll jeder Benutzer aufgelistet sein" do
        User.paginate(page: 1).each do |user|
          page.should have_selector('li', text: user.name)
        end
      end
    end

    describe "löschen-Links" do

      it { should_not have_link('löschen') }

      describe "als Admin" do
        let(:admin) { FactoryGirl.create(:admin) }
        before do
          sign_in admin
          visit users_path
        end

        it { should have_link('löschen', href: user_path(User.first)) }
        it "soll möglich sein einen anderen Benutzer zu löschen" do
          expect { click_link('löschen') }.to change(User, :count).by(-1)
        end
        it { should_not have_link('löschen', href: user_path(admin)) }
      end
    end
  end

  describe "Registrierungs-Seite" do
    before { visit registrierung_path }

    it { should have_selector('h1', :text => 'Registrierung') }
    it { should have_selector('title', :text => full_title('Registrierung')) }
  end

  describe "Registrierung" do
    before { visit registrierung_path }
    let(:submit) { "Account erstellen" }

    describe "mit ungültigen Informationen" do
      it "soll kein Benutzer erstellt werden" do
        expect { click_button submit }.not_to change(User, :count)
      end

      describe "nach Bestätigung" do
        before { click_button submit }

        it { should have_selector('title', text: 'Registrierung') }
        it { should have_content('Fehler') }
      end
    end

    describe "mit gültigen Informationen" do
      before do
        fill_in "Name",         with: "Toni Freitag"
        fill_in "E-Mail",       with: "toni@tonifreitag.de"
        fill_in "Passwort",     with: "foobar"
        fill_in "Bestätigung",  with: "foobar"
      end

      it "soll einen Benutzer erstellen" do
        expect { click_button submit }.to change(User, :count).by(1)
      end

      describe "nach Speicherung des Benutzers" do
        before { click_button submit }
        let(:user) { User.find_by_email('toni@tonifreitag.de') }

        it { should have_selector('title', text: user.name) }
        it { should have_selector('div.alert.alert-success', text: 'Willkommen') }
        it { should have_link('Abmelden') }

        describe "gefolgt vom Abmelden" do
          before { click_link "Abmelden" }
          it { should have_link('Anmelden') }
        end
      end
    end
  end

  describe "Profil-Seite" do
    let(:user) { FactoryGirl.create(:user) }
    before { visit user_path(user) }

    it { should have_selector('h1', text: user.name) }
    it { should have_selector('title', text: user.name) }
  end

  describe "bearbeiten" do
    let(:user) { FactoryGirl.create(:user) }
    before do
      sign_in user
      visit edit_user_path(user)
    end

    describe "Seite" do
      it { should have_selector('h1', text: "Profil aktualisieren") }
      it { should have_selector('title', text: "Benutzer bearbeiten") }
      it { should have_link('ändern', href: 'http://gravatar.com/emails') }
    end

    describe "mit ungültigen Informationen" do
      let(:new_name) { "Neuer Name" }
      let(:new_email) { "neu@tonifreitag.de" }
      before do
        fill_in "Name", with: new_name
        fill_in "E-Mail", with: new_email
        fill_in "Passwort", with: user.password
        fill_in "Passwort-Bestätigung", with: user.password
        click_button "Änderungen speichern"
      end

      it { should have_selector('title', text: new_name) }
      it { should have_selector('div.alert.alert-success') }
      it { should have_link('Abmelden', href: abmelden_path) }
      specify { user.reload.name.should == new_name }
      specify { user.reload.email.should == new_email }
    end
  end
end