# coding: utf-8

require 'spec_helper'

describe "Authentifikation" do
  
  subject { page }
  
  describe "Anmelden-Seite" do
    before { visit anmelden_path }

    it { should have_selector('h1', text: 'Anmelden') }
    it { should have_selector('title', text: 'Anmelden') }
  end
  
  describe "Anmelden" do
    before { visit anmelden_path }
    
    describe "mit ungültigen Informationen" do
      before { click_button "Anmelden" }
      
      it { should have_selector('title', text: 'Anmelden') }
      it { should have_selector('div.alert.alert-error', text: 'Ungültig') }
      
      describe "nach dem Besuch einer anderen Seite" do
        before { click_link "Startseite" }
        it { should_not have_selector('div.alert.alert-error') }
      end
    end
    
    describe "mit gültigen Informationen" do
      let(:user) { FactoryGirl.create(:user) }
      before { sign_in user }
      
      it { should have_selector('title', text: user.name) }
      
      it { should have_link('Benutzer', href: users_path) }
      it { should have_link('Profil', href: user_path(user)) }
      it { should have_link('Einstellungen', href: edit_user_path(user)) }
      it { should have_link('Abmelden', href: abmelden_path) }
      
      it { should_not have_link('Anmelden', href: anmelden_path) }
    end
  end
  
  describe "Autorisation" do
    
    describe "für nicht angemeldete Benutzer" do
      let(:user) { FactoryGirl.create(:user) }
      
      describe "wenn man eine geschützte Seite besuchen möchte" do
        before do
          visit edit_user_path(user)
          fill_in "Email",with: user.email
          fill_in "Password", with: user.password
          click_button "Anmelden"
        end
        
        describe "nach dem anmelden" do
          it "sollte man zur gewünschten Seite weitergeleitet werden werden" do
            page.should have_selector('title', text: 'Benutzer bearbeiten')
          end
        end
      end
    
      describe "im Users Controller" do
        describe "visiting the edit page" do
          before { visit edit_user_path(user) }
          
          it { should have_selector('title', text: 'Anmelden') }
        end
        
        describe "übertragen des Update Aktion" do
          before { put user_path(user) }
          specify { response.should redirect_to(anmelden_path) }
        end
        
        describe "besuche den Users-Index" do
          before { visit users_path }
          
          it { should have_selector('title', text: 'Anmelden') }
        end
      end
    end
    
    describe "als falscher Benutzer" do
      let(:user) { FactoryGirl.create(:user) }
      let(:wrong_user) { FactoryGirl.create(:user, email: "wrong@example.com") }
      before { sign_in user }
      
      describe "Besuche Users#edit Seite" do
        before { visit edit_user_path(wrong_user) }
        
        it { should_not have_selector('title', text: full_title('Benutzer bearbeiten')) }
      end
      
      describe "sende eine PUT-Anfrage an Users#update Aktion" do
        before { put user_path(wrong_user) }
        specify { response.should redirect_to(root_path) }
      end
    end
  
    describe "als nicht-admin Benutzer" do
      let(:user) { FactoryGirl.create(:user) }
      let(:non_admin) { FactoryGirl.create(:user) }
      
      before { sign_in non_admin }
      
      describe "übermittle eine DELETE Anfrage an die Users#destroy Aktion" do
        before { delete user_path(user) }
        specify { response.should redirect_to(root_path) }
      end
    end
  end
end