require 'spec_helper'

describe ApplicationHelper do
  describe "full_title" do
    it "Es soll den Seiten-Titel enthalten" do
      full_title("foo").should =~ /foo/
    end
    it "Es soll den Standard-Titel enthalten" do
      full_title("foo").should =~ /^Judo-Homepage/
    end
    it "Startseite soll keinen Titel enthalten" do
      full_title("").should_not =~ /\|/
    end
  end
end