module ApplicationHelper
  
  # Gibt den vollen Titel für einzelne Seiten zurück
  def full_title(page_title)
    base_title = "Judo-Homepage"
    if page_title.empty?
    base_title
    else
      "#{base_title} | #{page_title}"
    end
  end
end
